import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListStudentsComponent } from './components/list-students/list-students.component';
import { AddStudentComponent } from './components/add-student/add-student.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SearchbyLastnameComponent } from './components/searchby-lastname/searchby-lastname.component';

@NgModule({
  declarations: [
    AppComponent,
    ListStudentsComponent,
    AddStudentComponent,
    SearchbyLastnameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
