import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddStudentComponent } from './components/add-student/add-student.component';
import { ListStudentsComponent } from './components/list-students/list-students.component';

const routes: Routes = [
  {path: 'students', component: ListStudentsComponent},
  {path: 'add-student', component: AddStudentComponent},
  {path: 'edit-student/:id', component: AddStudentComponent},
  {path: '', redirectTo: '/students', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
