import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/models/Student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {

  students: Student[] = [];

  constructor(private studentSvc: StudentService) { }

  ngOnInit(): void {

    this.listStudents();
    
  } //end of ngOnInit

  listStudents() {
    this.studentSvc.getStudents().subscribe(
      data => this.students = data 
    )
  }

  deletedStudent(id: number) {
    this.studentSvc.deleteStudent(id).subscribe(
      data => this.listStudents()
    )
  }

} // end of class
