import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchbyLastnameComponent } from './searchby-lastname.component';

describe('SearchbyLastnameComponent', () => {
  let component: SearchbyLastnameComponent;
  let fixture: ComponentFixture<SearchbyLastnameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchbyLastnameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchbyLastnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
