import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../models/Student';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private getUrl: string = "http://localhost:8080/api/v1/students";

    constructor(private httpClient: HttpClient) { }

    getStudents(): Observable<Student[]> {
      return this.httpClient.get<Student[]>(this.getUrl).pipe(
        map (result => result)
      )
    }

    saveStudent(newStudent: Student): Observable<Student> {
      return this.httpClient.post<Student>(this.getUrl, newStudent);
    }

    viewStudent(id: number): Observable<Student> {
      return this.httpClient.get<Student>(`${this.getUrl}/${id}`).pipe(
        map(result => result)
      )
    }

    deleteStudent(id: number): Observable<any> {
      return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: "text"})
    }
    
} // end of class
